# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Žan Horvat, Filip Grčar, Anže Gorjan Novak in Gašper Vrhovnik |
| **Kraj in datum** | Ljubljana, 8.4.2019 |



## Povzetek projekta

V dokumentu je v uvodu predstavljena ideja aplikacije za študente StraightAs, nato so definirane štiri uporabniške vloge, ki so neprijavljen uporabnik, free in premium študent ter administrator. Za tem je predstavljen slovar ključnih besed, ki so uporabljene v dokumentu. Sledi diagram primerov uporabe, ki povzema vse funkcionalne zahteve opisane v nadaljevanju, kjer je opisanih 22 funkcionalnih zahtev, ki zajemajo delo z aktivnostmi, TO-DO seznami, koledarjem, upravljanje z ocenami predmetov ter vse funkcionalnosti, ki so pogosto prisotne pri spletnih aplikacijah kot so prijava, odjava, registracija in adminov vmesnik. Te vsebujejo osnovne, alternativne in izjemne tokove ter so različnih prioritet po principu MoSCoW. Opisani so tudi sprejemni testi. Sledijo nefunkcionalne zahteve kot npr. dostopnost sistema, robustnost in omejevanje dostopa. V nadaljevanju so predstavljeni še prototipi vmesnikov aplikacije ter nazadnje še opisi zunanjih vmesnikov kot je Google koledar.



## 1. Uvod

Aplikacija StraightAs je namenjena organizaciji dela pri študiju ter tako nudi podporo študentom pri opravljanju vsakodnevnih študijskih in tudi izvenštudijskih obveznosti. Aplikacija bo uporabniku omogočala vnos različnih aktivnosti kot so izpiti, kolokviji, oddaje domačih nalog in poljubnih ostalih aktivnosti. Študent bo lahko aktivnosti določal datume zapadlosti in prioriteto ter svoje opombe. Na podlagi oddaljenosti aktivnosti in prioritete si bo študent lahko generiral "TO-DO" sezname za stvari, ki jih mora opraviti posamezen dan. Ob opravljanju obveznosti bo nato kljukal stvari, ki jih je že opravil, ter spremljal napredek narejenih opravil na seznamu. V aplikaciji bo študent hranil tudi urnik ter si tako lažje uskladil predavanja in vaje z ostalimi obveznostmi. Omogočen bo tudi vnos ocen pri študijskih predmetih.



## 2. Uporabniške vloge

* **Neprijavljen uporabnik** ima dostop do funkcionalnosti prijava in registracija.
* **Študent (free)** ima na voljo funkcionalnosti v zvezi z upravljanjem aktivnosti, lahko upravlja s koledarjem, generira TO-DO sezname in vnaša ocene opravljenih aktivnosti. Prav tako si lahko spreminja lastne (prijavne) podatke. Seveda ima pravice do funkcionalsnosti odjave iz sistema.
* **Študent (premium)** lahko dodatno integrira svoj koledar z Google-ovim koledarjem ter z aplikacije so zanj odstranjeni vsi oglasi.
* **Admin** ima pooblastila za brisanje uporabnikov in njegovih podatkov.

V nadaljevanju dokumenta bo pojem **Študent** veljal tako za **free** kot tudi za **premium** študenta, razen če ni navedeno drugače. Uporabnik se navezuje vlogo, ki je omenjena v povzetku funkcionalnosti. Admin ima možnost spreminjanja vlog študentom - iz "free" na "premium" in obratno.



## 3. Slovar pojmov

* **TO-DO seznam** je skupek aktivnosti generiranih na podlagi koledarja, ki so predvidene za opravljanje na izbran dan.
* **Aktivnost** je označba na koledarju ali postavka v TO-DO seznamu z znanim začetkom in koncem trajanja aktivnosti (datum in čas), obsegom, oznako in označbo napredka pri opravljanju.
* **Koledar** je sistematična razdelitev leta na dneve, tedne in mesece, ki za določene dneve vsebuje aktivnosti.
* **Obseg** je ocena kompleksnosti in časovne zahtevnosti določene aktivnosti.
* **Oznaka aktivnosti** je oznaka, ki doda aktivnosti dodaten pomen.
* **Označba napredka pri opravljanju** je podatek v odstotkih (%), ki prikazuje količino opravljenega dela določene aktivnosti.
* **Obrazec** je način pridobivanja uporabnikovih informacij v sistemu, tak obrazec je sestavljen iz različnih vnosnih polj in potrdilnega gumba.



## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/UCD-v4.jpg)



## 5. Funkcionalne zahteve

### 1. Ustvarjanje aktivnosti

#### Povzetek funkcionalnosti

**Študent** lahko ustvarja aktivnosti.

#### Osnovni tok

1. Študent preko glavnega menija izbere funkcionalnost ustvarjanje aktivnosti.
2. Sistem prikaže obrazec za ustvarjanje aktivnosti.
3. Uporabnik izpolni zahtevana polja in svojo odločitev potrdi.
4. Sistem javi uporabniku o uspešno dodani aktivnosti.


#### Alternativni tok

**Alternativni tok 1**

1. Študent izbere dan na koledarju, kjer bi rad ustvaril neko aktivnost.
2. Sistem mu prikaže enak obrazec kot pri ustvarjanju aktivnosti, le da ima ta že izpolnjen datum zaključka te aktivnosti.
3. Uporabnik izpolni zahtevana polja in svojo odločitev potrdi.
4. Sistem javi uporabniku o uspešno dodani aktivnosti, ta se prikaže na koledarju.

#### Izjemni tok

- Pri shranjevanju aktivnosti pride do napake. Sistem opozori uporabnika in ponudi ponoven vnos.

#### Pogoji

* Vloga uporabnika mora biti **Študent**.


#### Posledice

* Ustvarjena je nova aktivnost.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                | Začetno stanje sistema                              | Vhod                                                         | Pričakovan rezultat               |
| -------------------------------------- | --------------------------------------------------- | ------------------------------------------------------------ | --------------------------------- |
| Ustvarjanje aktivnosti                 | Sistem s prijavljenim uporabnikom                   | Klik na gumb za kreiranje aktivnosti v glavnem meniju, vnos podatkov o aktivnosti in potrditev | Ustvarjena nova aktivnost         |
| Ustvarjanje aktivnosti preko koledarja | Sistem s prijavljenim uporabnikom prikazuje koledar | Klik na gumb za kreiranje aktivnosti na koledarju, vnos podatkov o aktivnosti in potrditev | Koledar z dodano novo aktivnostjo |



### 2. Urejanje aktivnosti

#### Povzetek funkcionalnosti

**Študent** lahko ureja aktivnosti.

#### Osnovni tok

1. Študent na koledarju izbere aktivnost, ki jo želi urediti.
2. Sistem prikaže formo za ustvarjanje aktivnosti, ki je predizpolnjena s podatki glede aktivnosti.
3. Uporabnik uredi željena polja in svojo odločitev potrdi.
4. Sistem javi uporabniku o uspešno urejeni aktivnosti.
5. Sistem prikaže posodobljen koledar.


#### Alternativni tok

**Alternativni tok 1**

1. Študent izbere aktivnost na TO-DO seznamu, ki bi jo rad uredil.
2. Sistem prikaže formo za ustvarjanje aktivnosti, ki je predizpolnjena s podatki glede aktivnosti.
3. Uporabnik uredi željena polja in svojo odločitev potrdi.
4. Sistem javi uporabniku o uspešno urejeni aktivnosti.
5. Sistem prikaže posodobljen TO-DO seznam.

#### Izjemni tokovi

- Uporabnik želi na koledarju urediti aktivnost, ki je že pretekla. Sistem izpiše ustrezno opozorilo.
- Pri shranjevanju urejene aktivnosti pride do napake. Sistem obvesti uporabnika in mu ponudi ponoven poskus.


#### Pogoji

* Vloga uporabnika mora biti **Študent**.


#### Posledice

* Posodobljena aktivnost.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                 | Začetno stanje sistema                                   | Vhod                                                         | Pričakovan rezultat                     |
| --------------------------------------- | -------------------------------------------------------- | ------------------------------------------------------------ | --------------------------------------- |
| Urejanje aktivnosti preko TO-DO seznama | Sistem s prijavljenim uporabnikom prikazuje TO-DO seznam | Klik na aktivnost na seznamu, sprememba željenih podatkov in potrditev | TO-DO seznam s posodobljeno aktivnostjo |
| Urejanje aktivnosti preko koledarja     | Sistem s prijavljenim uporabnikom prikazuje koledar      | Klik na aktivnost v koledarju, sprememba željenih podatkov in potrditev | Koledar s posodobljeno aktivnostjo      |



### 3. Brisanje aktivnosti

#### Povzetek funkcionalnosti

**Študent** lahko briše aktivnosti.

#### Osnovni tok

1. Študent na koledarju izbere aktivnost, ki bi jo rad izbrisal.
2. Sistem prikaže okno za urejanje aktivnosti, kjer je tudi gumb za brisanje.
3. Uporabnik klikne gumb za brisanje in potrdi svojo izbiro.
4. Sistem javi uporabniku o uspešno izbrisani aktivnosti.
5. Sistem prikaže posodobljen koledar.

#### Alternativni tok

**Alternativni tok 1**

1. Študent na TO-DO seznamu izbere aktivnost, ki bi jo rad izbrisal.
2. Sistem prikaže okno za urejanje aktivnosti, kjer je tudi gumb za brisanje.
3. Uporabnik klikne gumb za brisanje in potrdi svojo izbiro.
4. Sistem javi uporabniku o uspešno izbrisani aktivnosti.
5. Sistem prikaže posodobljen TO-DO seznam.

#### Izjemni tok

* Pri brisanju aktivnosti pride do napake. Sistem obvesti uporabnika in ga vrne na zadnjo obiskano stran.

#### Pogoji

* Vloga uporabnika mora biti **Študent**.


#### Posledice

* Aktivnost je izbrisana.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                | Začetno stanje sistema                                   | Vhod                                                         | Pričakovan rezultat                    |
| -------------------------------------- | -------------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------- |
| Brisanje aktivnosti prek TO-DO seznama | Sistem s prijavljenim uporabnikom prikazuje TO-DO seznam | Izbira aktivnosti, klik na "izbriši" in potrditev            | TO-DO seznam brez izbrisane aktivnosti |
| Brisanje aktivnosti prek koledarja     | Sistem s prijavljenim uporabnikom prikazuje koledar      | Izbira aktivnosti na koledarju, klik na "izbriši" in potrditev | Koledar brez izbrisane aktivnosti      |



### 4. Pregled aktivnosti v določenem časovnem obdobju v obliki koledarja


#### Povzetek funkcionalnost

**Študent** lahko v koledarju vidi vse aktivnosti na tedenski ali mesečni ravni.


#### Osnovni tok

1. Študent izbere funkcionalnost pregleda koledarja.
2. Uporabnik določi časovno obdobje (1 teden, 1 mesec).
3. Sistem na koledarju prikaže aktivnosti, ki se končajo v tem obdobju.


#### Alternativni tok(ovi)

Funkcionalnost nima alternativnih tokov.

#### Izjemni tok

- Uporabnik želi pregled aktivnosti za obdobje, ki nima aktivnosti. Sistem prikaže ustrezno opozorilo.


#### Pogoji

* Uporabnik mora imeti vlogo **Študent**.


#### Posledice

* Prikaz vseh aktivnosti v tem obdobju na koledarju ali sporočilo, da v tem obdobju ni predvidenih aktivnosti.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira               | Začetno stanje sistema            | Vhod                                                      | Pričakovan rezultat                                   |
| ------------------------------------- | --------------------------------- | --------------------------------------------------------- | ----------------------------------------------------- |
| Pregled koledarja z aktivnostmi       | Sistem s prijavljenim uporabnikom | Zahteva po pregledu koledarja in izbira časovnega obdobja | Prikaz koledarja z aktivnostmi iz izbranega intervala |
| Pregled koledarja, ki nima aktivnosti | Sistem s prijavljenim uporabnikom | Zahteva po pregledu koledarja in izbira časovnega obdobja | Prikaz obvestila, da ni predvidenih obveznosti        |



### 5. Prijava uporabnika


#### Povzetek funkcionalnost

**Neprijavljen uporabnik** se lahko prijavi v sistem.


#### Osnovni tok

1. Neprijavljen uporabnik (že registriran) želi uporabiti funkcionalnost prijave.
2. Sistem odpre stran za prijavo.
3. Uporabnik vpiše svoje prijavne podatke in pritisne gumb "Prijavi".
4. Sistem prijavi uporabnika in mu prikaže začetno stran aplikacije za prijavljene uporabnike.

#### Alternativni tok(ovi)

Funkcionalnost nima alternativnih tokov.

#### izjemni tokovi

- Neregistriran uporabnik se želi prijaviti v aplikacijo. Sistem ga ne spusti naprej in izpiše obvestilo.
- Registriran uporabnik se zmoti pri vnosu podatkov pri prijavi. Sistem ga ne spusti naprej in izpiše obvestilo.


#### Pogoji

* Za prijavo mora biti uporabnik predhodno registriran in v vlogi neprijavljenega uporabnika.

#### Posledice

* Po končani prijavi je uporabniku dodeljena njegova vloga (free/premium uporabnik oz. admin).


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira      | Začetno stanje sistema                                | Vhod                                                         | Pričakovan rezultat                                          |
| ---------------------------- | ----------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Uspešna prijava uporabnika   | Sistem z neprijavljenim uporabnikom na začetni strani | Zahteva po prijavi, vnos zahtevanih podatkov in potrditev    | Prijavljen uporabnik z dodeljeno vlogo ter sistem na začetni strani za to vlogo |
| Neuspešna prijava uporabnika | Sistem z neprijavljenim uporabnikom na začetni strani | Zahteva po prijavi, vnos napačnih zahtevanih podatkov in potrditev | Zavrnjena prijava in izpis obvestila na strani za prijavo    |



### 6. Odjava uporabnika


#### Povzetek funkcionalnosti

**Študent** se lahko odjavi iz sistema.


#### Osnovni tok

1. Prijavljen uporabnik se želi odjaviti iz sistema. Uporabi funkcionalnost odjave.
2. Uporabnik kilikne na gumb "Odjava".
3. Sistem odjavi uporabnika in ga preusmeri na začetno stran za neprijavljene uporabnike.


#### Alternativni tok

**Alternativni tok 1**

1. Prijavljen uporabnik je dolgo časa neaktiven.
2. Sistem samodejno odjavi uporabnika in ga  ob naslednjem dostopu preusmeri na začetno stran za neprijavljene uporabnike.

#### izjemni tok

- Pri odjavi pride do napake. Sistem obvesti uporabnika in ga vrne na zadnjo obiskano stran.


#### Pogoji

* Za odjavo mora biti uporabnik v vlogi prijavljenega uporabnika.


#### Posledice

* Po končani odjavi je prijavljenemu uporabniku dodeljena vloga neprijavljenega uporabnika.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                   | Začetno stanje sistema                                 | Vhod                                                         | Pričakovan rezultat                                          |
| ----------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Odjava prijavljenega uporabnika           | Sistem s prijavljenim uporabnikom na katerikoli strani | Klik na gumb za odjavo                                       | Sistem odjavi uporabnika in se vrne na začetno stran za neprijavljene uporabnike |
| Samodejna odjava prijavljenega uporabnika | Sistem s prijavljenim uporabnikom na katerikoli strani | Premik na drugo stran ali zahteva po katerikoli funkcionalnosti po obdobju neaktivnosti | Sistem prikaže stran za prijavo, saj je zaradi neaktivnosti uporabnika odjavil |



### 7. Registracija uporabnika


#### Povzetek funkcionalnost

**Neprijavljen uporabnik** se lahko registrira.


#### Osnovni tok

1. Neprijavljen uporabnik proži zahtevo po registraciji.
2. Sistem mu prikaže stran z registracijskim obrazcem.
3. Uporabnik vpiše potrebne podatke in potrdi izbiro.
4. Sistem uporabnika registrira in odpre stran za prijavo.

#### Alternativni tok(ovi)

Aktivnost nima alternativnih tokov.

#### Izjemni tok

- V aplikacijo se želi registrirati uporabnik z podatki že obstoječega uporabnika. Sistem zavrne registracijo in prikaže obvestilo.


#### Pogoji

* Uporabnik je v vlogi neprijavljenega uporabnika.


#### Posledice

* Registriran in prijavljen uporabnik v vlogi študent.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema                                       | Vhod                                                         | Pričakovan rezultat                                     |
| ----------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------- |
| Uspešna registracija    | Sistem z neprijavljenim uporabnikom na začetni strani za neprijavljenega uporabnika | Zahteva za registracijo, vnos zahtevanih podatkov in potrditev registracije | Registriran nov uporabnik in prikazana stran za prijavo |
| Neuspešna registracija  | Sistem z neprijavljenim uporabnikom na začetni strani za neprijavljenega uporabnika | Zahteva za registracijo, vnos zahtevanih podatkov, katerih e-mail se ujema z že obstoječim uporabnikom in potrditev registracije | Izpis obvestila o napaki                                |



### 8. Pregled TO-DO seznama


#### Povzetek funkcionalnosti

**Študent** si lahko za posamezni dan organizira delo s pomočjo ogleda izbranega TO-DO seznama.

#### Osnovni tok

1. Uporabnik uporabi funkcionalnost pregleda TO-DO seznama.
2. Sistem iz koledarja prebere aktivnosti in izbere primerne za izbran dan in jih logično uredi.
3. Uporabnik si lahko ogleda sestavljen TO-DO seznam.


#### Alternativni tok

**Alternativni tok 1**

1. Uporabnik uporabi funkcionalnost pregleda TO-DO seznama in izbere dan.
2. Sistem iz koledarja prebere aktivnosti in izbere primerne za izbran dan in jih logično uredi.
3. Uporabnik si lahko ogleda sestavljen TO-DO seznam za izbran dan.

#### izjemni tokovi

- Uporabnik želi pregled TO-DO seznama za dan, ko ni na sporedu nobene aktivnosti. Sistem izpiše ustrezno opozorilo.
- Uporabnik želi pregled TO-DO seznama za pretekli datum. Sistem izpiše ustrezno opozorilo.


#### Pogoji

* Vloga uporabnika je študent.
* Vsaj ena aktivnost na izbran dan.


#### Posledice

* Prikazan je TO-DO list za izbran dan.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                                    | Začetno stanje sistema            | Vhod                                                         | Pričakovan rezultat                                      |
| ---------------------------------------------------------- | --------------------------------- | ------------------------------------------------------------ | -------------------------------------------------------- |
| Pregled TO-DO seznama za tekoči dan                        | Sistem s prijavljenim uporabnikom | Zahteva za pregled TO-DO seznama, ko ima tekoči dan predvidene aktivnosti | Prikaz načrta dela za tekoči dan v obliki TO-DO seznama  |
| Pregled TO-DO seznama za izbrani dan                       | Sistem s prijavljenim uporabnikom | Zahteva za pregled TO-DO seznama in izbira datuma, za katerega so predvidene aktivnosti | Prikaz načrta dela za izbrani dan v obliki TO-DO seznama |
| Pregled TO-DO seznama za dan, ko ni predvidenih aktivnosti | Sistem s prijavljenim uporabnikom | Zahteva za pregled TO-DO seznama in izbira datuma, za katerega ni predvidenih aktivnosti | Prikaz obvestila, da ni predvidenih aktivnosti           |



### 9. Vnos označbe napredka pri opravljanju aktivnosti


#### Povzetek funkcionalnosti

**Študent** lahko aktivnostim sproti spreminja označbo o napredku aktivnosti (v odstotkih), prav tako ima možnost aktivnost označiti kot opravljeno neposredno iz TO-DO seznama. To pomeni da je označba avtomatsko nastavljena na 100%.

#### Osnovni tok

1. Uporabnik se nahaja na strani TO-DO seznama in s kljukico označi storitev kot opravljeno.
2. Sistem povpraša uporabnika za potrditev oznake v oknu z vpisanim 100% napredkom.
3. Uporabnik potrdi oznako.
4. Sistem prikaže isti TO-DO seznam brez dane aktivnosti.

#### Alternativna tokova

**Alternativni tok 1**

1. Uporabnik se nahaja na strani TO-DO seznama in zahteva vnos napredka aktivnosti s klikom na aktivnost.
2. Sistem prikaže obrazec za urejanje aktivnosti, kjer je možno vpisati tudi napredek.
3. Uporabnik vnese označbo napredka in jo potrdi.
4. Sistem prikaže TO-DO seznam z posodobljeno označbo napredka aktivnosti.

**Alternativni tok 2**

1. Uporabnik se nahaja na strani koledarja in zahteva vnos napredka aktivnosti s klikom na aktivnost.
2. Sistem prikaže obrazec za urejanje aktivnosti, kjer je možno vpisati tudi napredek.
3. Uporabnik vnese označbo napredka in jo potrdi.
4. Sistem prikaže koledar s posodobljeno označno napredka dane aktivnosti.

#### Izredna tokova

- Uporabnik želi vnesti označbo aktivnosti, ki je že bila opravljena. Sistem prikaže ustrezno obvestilo.
- Uporabnik želi vnesti označbo aktivnosti, ki je že pretekla. Sistem prikaže ustrezno obvestilo.

#### Pogoji

* Vloga uporabnika je študent.
* Aktivnost mora še potekati (datum konca aktivnosti še ni pretekel).
* Aktivnost ne sme biti že opravljena.


#### Posledice

* Nova označba napredka pri opravljanju aktivnosti.


#### Posebnosti

* Nima posebnih zahtev


#### Prioritete identificiranih funkcionalnosti

**MUST have**

#### Sprejemni testi

| Funkcija, ki se testira                                      | Začetno stanje sistema                                     | Vhod                                                         | Pričakovan rezultat                                          |
| ------------------------------------------------------------ | ---------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Označba opravljene aktivnosti na TO-DO seznamu               | Sistem s prijavljenim uporabnikom na prikazu TO-DO seznama | Označba aktivnosti na seznamu kot opravljena in potrditev označbe | Aktivnost ima posodobljeno označbo napredka na 100%, prikaz TO-DO seznama brez dane aktivnosti |
| Vnos označbe napredka aktivnosti na TO-DO seznamu            | Sistem s prijavljenim uporabnikom na prikazu TO-DO seznama | Izbira aktivnosti s seznama in vnos označbe napredka v obrazec za urejanje v odstotkih ter potrditev označbe | Aktivnost ima posodobljeno označbo napredka, prikaz osveženega TO-DO seznama |
| Vnos označbe napredka aktivnosti preko koledarja             | Sistem s prijavljenim uporabnikom na prikazu koledarja     | Izbira aktivnosti s koledarja in vnos označbe napredka v obrazec za urejanje v odstotkih ter potrditev označbe | Aktivnost ima posodobljeno označbo napredka, prikaz osveženega koledarja |
| Vnos označbe napredka aktivnosti preko koledarja za preteklo aktivnost | Sistem s prijavljenim uporabnikom na prikazu koledarja     | Izbira pretekle aktivnosti s koledarja in vnos označbe napredka v obrazec za urejanje v odstotkih ter potrditev označbe | Posodobitev se ne upošteva, prikaže pa se obvestilo o pretekli aktivnosti |
| Vnos označbe napredka aktivnosti preko koledarja za opravljeno aktivnost | Sistem s prijavljenim uporabnikom na prikazu koledarja     | Izbira opravljene aktivnosti s koledarja in vnos označbe napredka v obrazec za urejanje v odstotkih ter potrditev označbe | Posodobitev se ne upošteva, prikaže pa se obvestilo o opravljeni aktivnosti |



### 10. Uvoz zunanjih urnikov in koledarjev

#### Povzetek funkcionalnosti

**Študent** lahko v aplikacijo uvozi urnik ali koledar iz drugih platform kot so Fri urnik, Moodle in podobno, v formatu iCalendar.

#### Osnovni tok

1. Študent izbere funkcionalnost uvoza urnika ali koledarja.
2. Sistem prikaže obrazec za nalaganje datoteke.
3. Študent izbere datoteko v formatu iCalendar in potrdi obrazec.
4. Sistem razčleni datoteko in pretvori dogodke, zapisane v datoteki, v aktivnosti.
5. Sistem obvesti uporabnika o uspešnem uvozu dogodkov iz naložene datoteke.

#### Alternativni tok

- Funkcionalnost nima alternativnih tokov.

#### izjemni tok

- Sistemu ne uspe pravilno razčleniti datoteke ker ni v formatu iCalendar, zato javi napako.

#### Pogoji

- Vloga uporabnika je študent.

#### Posledice

- Dodane nove aktivnosti, ki so bile razbrane iz naložene datoteke.

#### Posebnosti

- Datoteka mora biti v formatu iCalendar

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira          | Začetno stanje sistema                                | Vhod                                                         | Pričakovan rezultat                                       |
| -------------------------------- | ----------------------------------------------------- | ------------------------------------------------------------ | --------------------------------------------------------- |
| Zapis aktivnosti v sistem | Sistem s prijavljenim študentom. | Izbira funkcionalnosti uvoza urnika ali koledarja, izbira datoteke v formatu iCalendar, potrditev nalaganja datoteke. | Sistem shrani nove aktivnosti, prebrane iz naložene datoteke in prikaže obvestilo o uspešno dodanih novih aktivnostih. |


### 11. Izvoz koledarja

#### Povzetek funkcionalnosti

**Študent** si lahko izvozi svoje aktivnosti iz aplikacije v formatu iCalendar.

#### Osnovni tok

1. Študent izbere funkcionalnost izvoza koledarja.
2. Sistem iz koledarja prebere vse aktivnosti študenta in jih pretvori format iCalendar.
3. Študent prenese datoteko v kateri so pretvorjene aktivnosti.

#### Alternativni tok

Funkcionalnost nima alternativnih tokov.

#### izjemni tok

- Študent želi izvoziti koledar, vendar nima shranjenih nobenih aktivnosti. Sistem uporabnika obvesti o napaki.

#### Pogoji

- Vloga uporabnika je študent.

#### Posledice

- Ustvarjena datoteka v formatu iCalendar, ki jo uporabnik shrani lokalno.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**SHOULD have**

#### Sprejemni testi

| Funkcija, ki se testira       | Začetno stanje sistema                                | Vhod                        | Pričakovan rezultat              |
| ----------------------------- | ----------------------------------------------------- | --------------------------- | -------------------------------- |
| Izvoz aktivnosti iz aplikacije | Sistem s prijavljenim študentom, ki ima vsaj eno shranjeno aktivnost | Zahteva po izvozu koledarja | Sistem ponudi datoteko za prenos |
| Izvoz 0 aktivnosti iz aplikacije | Sistem s prijavljenim študentom, ki nima shranjenih aktivnosti | Zahteva po izvozu koledarja | Sistem javi napako pri izvažanju |



### 12. Iskanje aktivnosti glede na naziv in oznako


#### Povzetek funkcionalnosti

**Študent** lahko s pomočjo iskanja poišče aktivnosti, ki ustrezajo iskalnemu nizu.


#### Osnovni tok

1. Študent želi poiskati aktivnosti, ki v nazivu ali oznaki vsebujejo določen niz. Uporabi funkcionalnost iskanje aktivnosti glede na naziv in oznako.
2. Uporabnik v meniju pritisne na napis "Iskanje".
3. Sistem uporabniku stran z vnosnim poljem in gumbom za sprožitev iskanja.
4. Uporabnik izpolni potrebna polja in sproži iskanje.
5. Na strani se prikažejo rezultati iskanja.

#### Alternativni tok(ovi)

**Alternativni tok 1**

1. Študent želi poiskati aktivnosti, ki v nazivu ali oznaki vsebujejo določen niz. Uporabi funkcionalnost iskanje aktivnosti glede na naziv in oznako.
2. Uporabnik v meniju pritisne na napis "Iskanje".
3. Sistem uporabniku stran z vnosnim poljem in gumbom za sprožitev iskanja.
4. Uporabnik izpolni potrebna polja in sproži iskanje.
5. Na strani se prikažejo obvestilo, da ni bilo najdenih rezultatov.

#### izjemni tok

- Pri iskanju pride do napake. Sistem vrne ustreno obvestilo in vrne uporabnika nazaj na stran za iskanje.


#### Pogoji

* Uporabnik mora biti prijavljen in vnosna polja morajo za iskanje vsebovati ustrezne vnose.


#### Posledice

* Po iskanju so uporabniku prikazani rezultati ali obvestilo, da ni rezultatov.


#### Posebnosti

* Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira                     | Začetno stanje sistema            | Vhod                                                         | Pričakovan rezultat                                          |
| ------------------------------------------- | --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Iskanje aktivnosti glede na naziv in oznako | Sistem s prijavljenim uporabnikom | Zahteva po iskanju aktivnosti, vnos ustreznega niza in potrditev iskanja | Prikaz zahtevanih aktivnosti glede na vnešen niz ali sporočilo, da ni najdenih ustreznih aktivnosti |
| Iskanje aktivnosti glede na naziv in oznako | Sistem s prijavljenim uporabnikom | Zahteva po iskanju aktivnosti in potrditev iskanja brez vnešenega niza | Prikaz opozorila in ponoven prikaz okna za vnos niza         |



### 13. Avtomatsko resetiranje gesla

#### Povzetek funkcionalnosti

**Neprijavljen uporabnik** lahko zahteva resetiranje gesla, če ga je npr. pozabil.

#### Osnovni tok

1. Sistem s neprijavljenim uporabnikom se nahaja na strani prijave.
2. Uporabnik zahteva resetiranje gesla.
3. Sistem zgenerira novo geslo in ga shrani.
4. Uporabnik prejme e-mail z novim geslom.
5. Sistem prikaže obvestilo o uspešni ponastavitvi gesla.

#### Alternativni tok(ovi)

Funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

- Neregistriran uporabnik zahteva resetiranje gesla. Sistem izpiše opozorilo.
- Pri generiranju ali shranjevanju gesla pride do napake. Sistem izpiše opozorilo.
- Uporabnik ne prejme e-maila z novim geslom in mora ponovno zahtevati ponastavitev gesla.

#### Pogoji

- Neprijavljen (in registriran) uporabnik, ki pravilno vpiše svoje uporabniško ime.

#### Posledice

- Ponastavljeno geslo določenega uporabnika.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**SHOULD have**

#### Sprejemni testi

| Funkcija, ki se testira      | Začetno stanje sistema                   | Vhod                                                         | Pričakovan rezultat                                          |
| ---------------------------- | ---------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Avtomatsko resetiranje gesla | Neprijavljen uporabnik na strani prijave | Uporabniško ime registriranega uporabnika in zahteva za resetiranje gesla | Sistem zgenerira in shrani novo geslo, uporabnik pa prejme e-mail z novim geslom |



### 14. Spreminjanje podatkov uporabnikov

#### Povzetek funkcionalnosti

**Admin** lahko spreminja in ureja podatke uporabnikov.

#### Osnovni tok

1. Sistem s prijavljenim adminom se nahaja na strani s seznamom vseh uporabnikov.
2. Admin izbere svoj račun ali račun določenega uporabnika.
3. Sistem prikaže obrazec za spremembo podatkov uporabnika z izpolnjenimi polji s podatki uporabnika.
4. Admin vnese željene spremembe.
5. Admin potrdi spremembe.
6. Sistem shrani spremembe.
7. Sistem prikaže obvestilo o uspešnem vnosu sprememb.

#### Alternativni tok(ovi)

Funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

- Pri samodejnem izpolnjevanju obrazca za spremembe podatkov pride do napake, ker sistem ne more pridobiti vseh podatkov. Sistem opozori admina in ga vrne na stran za urejanje podatkov o uporabnikih.
- Pri shranjevanju posodobljenih podatkov o uporabniku pride do napake. Sistem opozori admina in ga vrne na obrazec z vnesenimi obstoječimi podatki.

#### Pogoji

- Prijavljen admin.

#### Posledice

- Spremenjeni podatki o določenem uporabniku.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**SHOULD have**

#### Sprejemni testi

| Funkcija, ki se testira          | Začetno stanje sistema                 | Vhod                                                         | Pričakovan rezultat             |
| -------------------------------- | -------------------------------------- | ------------------------------------------------------------ | ------------------------------- |
| Spreminjanje podatkov uporabnika | Admin na strani s seznamom uporabnikov | Izbira uporabnika, sprememba nekaterih podatkov in potrditev sprememb | Posodobljeni podatki uporabnika |



### 15. Izbris računa uporabnika

#### Povzetek funkcionalnosti

**Admin** lahko izbriše račun poljubnega uporabnika.

#### Osnovni tok

1. Sistem s prijavljenim adminom se nahaja na strani s seznamom vseh uporabnikov.
2. Admin izbere račun določenega uporabnika.
3. Sistem prikaže obrazec za spremembo podatkov uporabnika z izpolnjenimi polji s podatki uporabnika.
4. Admin izbere gumb za izbris računa.
5. Admin potrdi izbris.
6. Sistem izbriše račun uporabnika.
7. Sistem prikaže obvestilo o uspešnem izbrisu računa.

#### Alternativni tok(ovi)

Funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

- Pri brisanju računa pride do napake. Sistem prikaže opozorilo in se vrne na stran s seznamom uporabnikov.

#### Pogoji

- Prijavljen admin.

#### Posledice

- Izbrisan račun določenega uporabnika.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira  | Začetno stanje sistema                 | Vhod                                                         | Pričakovan rezultat                                          |
| ------------------------ | -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Izbris računa uporabnika | Admin na strani s seznamom uporabnikov | Izbira uporabnika, izbira gumba za izbris in potrditev sprememb | Izbrisan račun uporabnika in prikazan posodobljen seznam uporabnikov |



### 16. Vklop sinhronizacije z Google koledarjem

#### Povzetek funkcionalnosti

**Študent (premium)** ki še nima aktivirane sinhronizacije, lahko vklopi sinhronizacijo aktivnosti z Google koledarjem. Aktivnosti so v Google koledarju predstavljene kot dogodki.

#### Osnovni tok

1. Študent (premium), ki še nima aktivirane sinhronizacije, in je že prijavljen z Google računom izbere funkcionalnost "sinhronizacija z Google koledarjem".
2. Sistem prikaže okno za dostop aplikacije StragihtAs do študentovih Google koledarjev.
3. Študent z izbiro potrdi dostop.
4. Sistem prikaže obrazec za izbiro Google koledarja s katerim se bodo sinhronizirale aktivnosti.
5. Študent izbere Google koledar za sinhronizacijo.
6. Sistem javi uspešen vklop sinhronizacije.

#### Alternativni tok

**Alternativni tok 1**

1. Študent (premium), ki še nima aktivirane sinhronizacije, in še ni prijavljen z Google računom izbere funkcionalnost "sinhronizacija z Google koledarjem".
2. Sistem prikaže okno za prijavo v Google račun.
3. Študent se prijavi v Google račun.
4. Sistem prikaže okno za dostop aplikacije StragihtAs do študentovih Google koledarjev.
5. Študent z izbiro potrdi dostop.
6. Sistem prikaže obrazec za izbiro Google koledarja s katerim se bodo sinhronizirale aktivnosti.
7. Študent izbere Google koledar za sinhronizacijo.
8. Sistem javi uspešen vklop sinhronizacije.

#### Izjemni tok

- Študent zavrne dostop aplikacije StragihtAs do njegovega koledarja. Sistem opozori uporabnika.

#### Pogoji

- Vloga uporabnika mora biti **Študent (premium)** in ne sme imeti že aktivirane sinhronizacije z Google koledarjem.
- Uporabnik mora imeti Google račun.

#### Posledice

- Ko je uporabnik prijavljen, sistem v ozadju avtomatsko sinhronizira aktivnosti z dogodki v izbranem Google koledarju.


#### Posebnosti

- Funkcionalnost uporablja zunanji sistem Google Calendar API in mora upoštevati njegove specifikacije.


#### Prioritete identificiranih funkcionalnosti

**SHOULD have**

#### Sprejemni testi

| Funkcija, ki se testira             | Začetno stanje sistema                              | Vhod                                                         | Pričakovan rezultat                |
| ----------------------------------- | --------------------------------------------------- | ------------------------------------------------------------ | ---------------------------------- |
| Prikaz obvestila za uspešni vklop sinhronizacije | Sistem s prijavljenim uporabnikom z vlogo Študent (premium), ki še nima aktivirane sinhronizacije z Google koledarjem. Uporabnik mora imeti Google račun. | Izbira funkcionalnosti, (vnos uporabniškega imena in gesla in potrditev prijave - v primeru da uporabnik še ni prijavljen), izbira gumba za potrditev dostopa, izbira koledarja za sinhronizacijo.  | Obvestilo o uspešnem vklopu sinhronizacije. |
| Začetek sinhronizacije. | Sistem s prijavljenim uporabnikom z vlogo Študent (premium), ki še nima aktivirane sinhronizacije z Google koledarjem. Uporabnik mora imeti Google račun. | Izbira funkcionalnosti, (vnos uporabniškega imena in gesla in potrditev prijave - v primeru da uporabnik še ni prijavljen), izbira gumba za potrditev dostopa, izbira koledarja za sinhronizacijo.  | Sistem začne z začetno sinhronizacijo in vklopi sprotno sinhronizacijo. |
| Prikaz opozorila zavrnjenem dostopu. | Sistem s prijavljenim uporabnikom z vlogo Študent (premium), ki še nima aktivirane sinhronizacije z Google koledarjem. Uporabnik mora imeti Google račun. | Izbira funkcionalnosti, (vnos uporabniškega imena in gesla in potrditev prijave - v primeru da uporabnik še ni prijavljen), izbira gumba za zavrnitev dostopa.  | Opozorilo o zavrnjenem dostopu do uporabnikovih Google koledarjev. |



### 17. Izklop sinhronizacije z Google koledarjem

#### Povzetek funkcionalnosti

**Študent (premium)** ki že ima aktivirano sinhronizacijo, lahko izklopi sinhronizacijo aktivnosti z Google koledarjem.

#### Osnovni tok

1. Študent (premium), ki že ima aktivirano sinhronizacijo, izbere funkcionalnost "sinhronizacija z Google koledarjem".
2. Sistem prikaže okno za potrditev izklopa sinhronizacije.
3. Študent z izbiro potrdi izklop.
4. Sistem javi uspešen izklop sinhronizacije.

#### Alternativni tok(ovi)

- Funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

- Funkcionalnost nima izjemnih tokov.

#### Pogoji

- Vloga uporabnika mora biti **Študent (premium)** in mora že imeti aktivirano sinhronizacijo z Google koledarjem.

#### Posledice

- Sistem izklopi avtomatsko sinhronizacijo aktivnosti z dogodki v Google koledarju.


#### Posebnosti

- Funkcionalnost nima posebnosti.


#### Prioritete identificiranih funkcionalnosti

**SHOULD have**

#### Sprejemni testi

| Funkcija, ki se testira             | Začetno stanje sistema                              | Vhod                                                         | Pričakovan rezultat                |
| ----------------------------------- | --------------------------------------------------- | ------------------------------------------------------------ | ---------------------------------- |
| Prikaz obvestila za uspešni izklop sinhronizacije | Sistem s prijavljenim uporabnikom z vlogo Študent (premium), ki že ima aktivirano sinhronizacijo z Google koledarjem. | Izbira funkcionalnosti, izbira gumba za potrditev izklopa.  | Obvestilo o uspešnem izklopu sinhronizacije. |
| Izklop avtomatske sinhronizacije. | Sistem s prijavljenim uporabnikom z vlogo Študent (premium), ki že ima aktivirano sinhronizacijo z Google koledarjem. | Izbira funkcionalnosti, izbira gumba za potrditev izklopa.  | Sistem izklopi sprotno sinhronizacijo. |



### 18. Pregled predmetov z vpisanimi ocenami

#### Povzetek funkcionalnosti

**Študent** lahko pregleda seznam svojih predmetov z vpisanimi ocenami.

#### Osnovni tok

1. Študent zahteva prikaz predmetov z vpisanimi ocenami.
2. Sistem prikaže seznam predmetov z morebitnimi ocenami.

#### Alternativni tok(ovi)

- Funkcionalnost nima alternativnih tokov.

#### Izjemna tokova

- Ko študent zahteva prikaz predmetov z ocenami in nima zabeleženega niti enega predmeta, se izpiše obvestilo, da ni vnešenih predmetov in ocen.
- Pri pridobivanju predmetov ali ocen iz podatkoven baze pride do napake. Sistem opozori študenta in ga vrne na zadnjo obiskano stran.

#### Pogoji

- Vloga uporabnika mora biti **Študent**.

#### Posledice

- Prikazan seznam predmetov z morebitnimi ocenami.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira           | Začetno stanje sistema                                       | Vhod                                  | Pričakovan rezultat                                 |
| --------------------------------- | ------------------------------------------------------------ | ------------------------------------- | --------------------------------------------------- |
| Prikaz predmetov in ocen          | Sistem s prijavljenim uporabnikom z vlogo Študent na katerikoli strani. | Zahteva za pregled predmetov in ocen. | Prikazan seznam predmetov z morebitnimi ocenami.    |
| Prikaz opozorila, da ni predmetov | Sistem s prijavljenim uporabnikom z vlogo Študent, kateri nima vnešenega nobenega predmeta, na katerikoli strani. | Zahteva za pregled predmetov in ocen. | Prikaz obvestila, da ni vnešenih predmetov in ocen. |



### 19. Vnos ocene pri predmetih

#### Povzetek funkcionalnosti

**Študent** lahko za svoje predmete vnaša pridobljene ocene.

#### Osnovni tok

1. Študent na strani s predmeti in ocenami s klikom na gumb za dodajanje ocen zahteva vnos nove ocene.
2. Sistem prikaže okno s spustim seznamom s predmeti in možnostjo vpisa novega predmeta ter polje za vnos ocene med 1 in 10.
3. Študent izbere predmet in vpiše oceno.
4. Študent potrdi vnos.
5. Sistem shrani novo oceno.
6. Sistem prikaže posodobljen seznam predmetov in ocen.

#### Alternativni tok

**Alternativni tok 1**

1. Študent na strani s predmeti in ocenami s klikom na gumb za dodajanje ocen zahteva vnos nove ocene.
2. Sistem prikaže okno s spustim seznamom s predmeti in možnostjo vpisa novega predmeta ter polje za vnos ocene med 1 in 10.
3. Študent vpiše nov predmet in vpiše oceno.
4. Študent potrdi vnos.
5. Sistem shrani nov predmet in novo oceno.
6. Sistem shrani predmet tudi kot oznako aktivnosti.
7. Sistem prikaže posodobljen seznam predmetov in ocen.

#### Izjemni tok

- Pri pridobivanju predmeta ali ocen v podatkovno bazo pride do napake. Sistem opozori študenta in ga vrne nazaj na vnos ocene.

#### Pogoji

- Vloga uporabnika mora biti **Študent**.

#### Posledice

- Vnos nove ocene in morda tudi predmeta ter oznake za aktivnost.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira            | Začetno stanje sistema                                       | Vhod                                                         | Pričakovan rezultat                                          |
| ---------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Vnos ocene pri obstoječem predmetu | Sistem s prijavljenim uporabnikom z vlogo Študent na strani s predmeti in ocenami. | Klik na gumb za vnos ocen, izbira predmeta in vnos ocene.    | Shranjena nova ocena in posodobljen seznam predmetov z ocenami. |
| Vnos ocene in novega predmeta      | Sistem s prijavljenim uporabnikom z vlogo Študent na strani s predmeti in ocenami. | Klik na gumb za vnos ocen, vpis novega predmeta in vnos ocene. | Shranjen nov predmet z novo oceno ter nova oznaka aktivnosti z imenom predmeta ter posodobljen seznam predmetov z ocenami. |



### 20. Izbris ocene pri predmetih

#### Povzetek funkcionalnosti

**Študent** lahko za svoje predmete izbriše napačno vnešene ocene.

#### Osnovni tok

1. Študent na strani s predmeti in ocenami s klikom na določeno oceno zahteva izbris ocene.
2. Sistem prikaže okno za potrditev izbrisa.
3. Študent potrdi izbiro.
4. Sistem izbriše oceno.
5. Sistem prikaže posodobljen seznam predmetov in ocen.

#### Alternativni tok(ovi)

1. Funkcionalnost nima alternativnih tokov.

#### Izjemni tok

- Pri izbrisu ocene pride do napake. Sistem izpiše opozorilo in vrne študenta na seznam predmetov z ocenami.

#### Pogoji

- Vloga uporabnika mora biti **Študent**, kateri ima vpisano vsaj eno oceno.

#### Posledice

- Izbrisana izbrana ocena pri nekem predmetu.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema                                       | Vhod                                | Pričakovan rezultat                                        |
| ----------------------- | ------------------------------------------------------------ | ----------------------------------- | ---------------------------------------------------------- |
| Izbris ocene predmeta   | Sistem s prijavljenim uporabnikom z vlogo Študent na strani s predmeti in ocenami. | Klik na oceno in potrditev izbrisa. | Izbrisana ocena in posodobljen seznam predmetov z ocenami. |



### 21. Izbris predmeta na seznamu predmetov

#### Povzetek funkcionalnosti

**Študent** lahko izbriše predmete na seznamu predmetov.

#### Osnovni tok

1. Študent na strani s predmeti in ocenami s klikom na določen predmet zahteva izbris predmeta.
2. Sistem prikaže okno za potrditev izbrisa.
3. Študent potrdi izbiro.
4. Sistem izbriše vse ocene pri tem predmetu.
5. Sistem izbriše oznako aktivnosti z imenom predmeta in jo odstrani vsem aktivnostim, ki jo vsebujejo.
6. Sistem izbriše predmet.
7. Sistem prikaže posodobljen seznam predmetov z ocenami.

#### Alternativni tok

**Alternativni tok 1**

1. Študent na strani s predmeti in ocenami s klikom na edini predmet zahteva njegov izbris.
2. Sistem prikaže okno za potrditev izbrisa.
3. Študent potrdi izbiro.
4. Sistem izbriše vse ocene pri tem predmetu.
5. Sistem izbriše oznako aktivnosti z imenom predmeta in jo odstrani vsem aktivnostim, ki jo vsebujejo.
6. Sistem izbriše predmet.
7. Sistem prikaže obvestilo, da ni vpisanih predmetov.

#### Izjemni tok

- Pri izbrisu predmeta oz. povezanih ocen in oznake pride do napake. Sistem izpiše opozorilo in vrne študenta na seznam predmetov z ocenami.

#### Pogoji

- Vloga uporabnika mora biti **Študent**, kateri ima vpisan vsaj en predmet.

#### Posledice

- Izbrisan predmet s pripadajočimi ocenami in oznako aktivnosti.

#### Posebnosti

- Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

**COULD have**

#### Sprejemni testi

| Funkcija, ki se testira             | Začetno stanje sistema                                       | Vhod                                  | Pričakovan rezultat                                          |
| ----------------------------------- | ------------------------------------------------------------ | ------------------------------------- | ------------------------------------------------------------ |
| Izbris enega izmed mnogih predmetov | Sistem s prijavljenim uporabnikom z vlogo Študent na strani s predmeti in ocenami. | Klik na predmet in potrditev izbrisa. | Izbrisan predmet s pripadajočimi ocenami in oznako aktivnosti ter prikaz posodobljenega seznama predmetov z ocenami. |
| Izbris edinega predmeta             | Sistem s prijavljenim uporabnikom z vlogo Študent na strani s predmeti in ocenami. | Klik na predmet in potrditev izbrisa. | Izbrisan predmet s pripadajočimi ocenami in oznako aktivnosti ter prikaz obvestila, da ni vnesenih predmetov. |



### 22. Prikaz oglaševalskih vsebin

#### Povzetek funkcionalnosti

**Študent (free)** in **neprijavljen uporabnik** na vseh straneh aplikacije ob levem in desnem robu strani vidita oglaševalsko vsebino.

#### Osnovni tok

1. Študent (free) ali neprijavljen uporabnik dostopata do poljubne strani aplikacije.
2. Sistem prikaže stran z vključenimi oglasi.
3. Študent (free) ali neprijavljen uporabnik ob ogledu strani vidita tudi oglase.

#### Alternativni tok

- Aktivnost nima alternativnih tokov.

#### Izjemni tok

- Pri pridobivanju oglasov pride do napake. Stran se prikaže brez oglasov.

#### Pogoji

- Vloga uporabnika mora biti **Študent (free)** ali **neprijavljen uporabnik**.

#### Posledice

- Na straneh aplikacije ob vsebini prikazani oglasi.

#### Posebnosti

- Ob nalaganju strani se vključijo še oglasi iz oglaševalskega strežnika v za to namenjen prostor na strani.

#### Prioritete identificiranih funkcionalnosti

**WOULD have**

#### Sprejemni testi

| Funkcija, ki se testira                    | Začetno stanje sistema                                       | Vhod                                    | Pričakovan rezultat                                        |
| ------------------------------------------ | ------------------------------------------------------------ | --------------------------------------- | ---------------------------------------------------------- |
| Prikaz oglasov iz oglaševalskega strežnika | Sistem z neprijavljenim uporabnikom ali s prijavljenim z vlogo Študent (free). | Dostop do katerekoli strani aplikacije. | Ob vsebini strani prikazani oglasi iz oglasnega strežnika. |
| Prikaz fiksnih oglasov                     | Sistem z neprijavljenim uporabnikom ali s prijavljenim z vlogo Študent (free). | Dostop do katerekoli strani aplikacije. | Ob vsebini prikazani oglasi v obliki fiksnih slik.         |



## 6. Nefunkcionalne zahteve

* Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.
    * Sistem je namenjen javnosti, predvsem pa študentom in mora biti dosegljiv povsod, kjer je povezava do interneta.
    
* Sistem uporabniku ne sme omogočiti dostopa do podatkov, za katere ni izrecno pooblaščen.
    * Sistem mora omejiti dostop neprijavljenim uporabnikom do vsebin, ki jih lahko dostopajo prijavljeni uporabniki. Prav tako uporabniki ne smejo dostopati do aktivnosti drugih uporabnikov.

* Sistem ne sme hraniti odvečnih podatkov uporabnika, ostale mora hraniti varno. In ob zahtevi po izbrisu teh podakov to storiti.
    * Sistem mora slediti GDPR.

* Sistem mora biti na voljo najmanj 99,9 odstotkov časa.
    * Sistem mora biti dosegljiv vedno, saj je tako konkurenčen drugim produktom in izboljša uporabniško izkušnjo.
    
* Sistem mora biti zmožen streči najmanj 1000 hkratnim uporabnikom.
    * Sistem bo uporabljen s strani študentov, teh je veliko, zato mora biti odziven.




## 7. Prototipi vmesnikov

### Neprijavljeni

![Zaslonska maska - prva](../img/zaslonske_maske/welcome.JPG)


![Zaslonska maska - registracija](../img/zaslonske_maske/registracija.JPG)

**Funkcionalne zahteve:**
- Registracija

![Zaslonska maska - prijava](../img/zaslonske_maske/prijava.JPG)

**Funkcionalne zahteve:**
- Prijava

![Zaslonska maska - prijava](../img/zaslonske_maske/reset-gesla.JPG)

**Funkcionalne zahteve:**
- Avtomatsko resetiranje gesla

### Študenti

Veliko funkcionalnih zahtev je implementiranih v pomožnem meniju na straneh prijavljenih uporabnikov oz. študentov. To so sledeče:

* Ustvarjanje aktivnosti
* Pregled aktivnosti v določenem časovnem obdobju v obliki koledarja
* Uvoz zunanjih urnikov in koledarjev
* Izvoz koledarja
* Vklop sinhronizacije z Google koledarjem
* Izklop sinhronizacije z Google koledarjem
* Pregled predmetov z vpisanimi ocenami

![Zaslonska maska - koledar](../img/zaslonske_maske/pregled-todo.JPG)

**Funkcionalne zahteve:**
- Pregled TO-DO seznama
- Vnos označbe napredka

![Zaslonska maska - koledar](../img/zaslonske_maske/koledar.JPG)

**Funkcionalne zahteve:**
- Pregled aktivnosti na koledarju

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/dodaj-aktivnost.png)

**Funkcionalne zahteve:**
- Ustvarjanje aktivnosti

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/spremeni-aktivnost.png)

**Funkcionalne zahteve:**
- Urejanje aktivnosti

![Zaslonska maska - urejanje aktivnosti](../img/zaslonske_maske/iskanje-aktivnost.JPG)

**Funkcionalne zahteve:**
- Iskanje aktivnosti glede na naziv in oznake

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/uvoz.JPG)

**Funkcionalne zahteve:**
- Uvoz zunanjih urnikov in koledarjev

![Zaslonska maska - pregled ocen](../img/zaslonske_maske/pregled-ocen.png)

**Funkcionalne zahteve:**
- Pregled predmetov z vpisanimi ocenami


![Zaslonska maska - pregled ocen](../img/zaslonske_maske/vnos-ocene.png)

**Funkcionalne zahteve:**
- Vnos ocene pri predmetih

### Admin

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/dom-admin.JPG)

**Funkcionalne zahteve:**
- Izbris uporabnika

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/admin.JPG)

**Funkcionalne zahteve:**
- Spreminjanje podatkov

### Študent (free)

![Zaslonska maska - ustvarjanje aktivnosti](../img/zaslonske_maske/reklame.JPG)

**Funkcionalne zahteve:**

- Prikaz oglasnih vsebin

## 8. Vmesniki do zunanjih sistemov

Za sinhronizacijo z Google koledarjem so potrebne sledeče interakcije z Google Calendar API:

* Avtorizacija
* Pridobivanje dogodkov.
* Dodajane dogodkov.
* Brisanje dogodkov.

### Avtorizacija

Avtorizacija poteka preko splošno uveljavljenega protokola [OAuth 2.0](https://oauth.net/2/).

### Vmesnik za pridobivanje dogodkov

Sistem pošlje HTTP GET zahtevo Na naslov `https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events`, kjer je `{calendarId}` identifikator izbranega koledarja.

Če ne gre za prvo zahtevo za ta koledar, se poizvedbi doda tudi polje `syncToken`: 
`https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events?syncToken={syncToken}`.
Vrednost tega polja je shranjena od prejšnjega odgovora Google Calendar API strežnika.

Strežnik Google Calendar API odovori v sledečem formatu:

``` json
{
  "kind": "calendar#events",
  "etag": etag,
  "summary": string,
  "description": string,
  "updated": datetime,
  "timeZone": string,
  "accessRole": string,
  "defaultReminders": [
    {
      "method": string,
      "minutes": integer
    }
  ],
  "nextPageToken": string,
  "nextSyncToken": string,
  "items": [
    events Resource
  ]
}
```

[Podrobnejša dokumentacija: https://developers.google.com/calendar/v3/reference/events/list](https://developers.google.com/calendar/v3/reference/events/list)

### Vmesnik za dodajanje dogodkov

Sistem pošlje HTTP POST zahtevo Na naslov `POST https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events`, kjer je `{calendarId}` identifikator izbranega koledarja.

Telo zahteve:

``` json
{
  "summary" : string,
  "description": string,
  "start": {
    "dateTime": datetime
  },
  "end": {
    "dateTime": datetime
  }
}
```

| POLJE          | OPIS                            |
|----------------|---------------------------------|
| summary        | ime aktivnosti                  |
| description    | opis aktivnos (obsežnost)       |
| start.dateTime | datum in čas začetka aktivnosti |
| end.dateTime   | datum in čas konca aktivnosti   |

Če se je aktivnost uspešno shranila kot dogodek v Google koledarju, strežnik Google Calendar API odovori s HTTP kodo 201.

[Podrobnejša dokumentacija: https://developers.google.com/calendar/v3/reference/events/insert](https://developers.google.com/calendar/v3/reference/events/insert)

### Vmesnik za brisanje dogodkov

Sistem pošlje HTTP DELETE zahtevo Na naslov `https://www.googleapis.com/calendar/v3/calendars/{calendarId}/events/{eventId}`, kjer je `{calendarId}` identifikator izbranega koledarja in `{eventId}` identifikator dogodka.

Če se je dogodek uspešno izbrisal, strežnik Google Calendar API odovori s HTTP kodo 204.

[Podrobnejša dokumentacija: https://developers.google.com/calendar/v3/reference/events/delete](https://developers.google.com/calendar/v3/reference/events/delete)
